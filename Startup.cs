﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(QueueLog.Startup))]
namespace QueueLog
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
